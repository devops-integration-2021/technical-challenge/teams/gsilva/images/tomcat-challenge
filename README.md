#Creacion image tomcat-challenge

git clone https://gitlab.com/devops-integration-2021/container/docker-project/tomcat9-openjdk8-centos7.git

cd tomcat9-openjdk8-centos7

docker build -t tomcat9:openjdk8-centos7 .

docker run -it -d -p 9900:8080 --name tomcat-challenge tomcat9:openjdk8-centos7

URL: http://192.168.100.80:9900/
